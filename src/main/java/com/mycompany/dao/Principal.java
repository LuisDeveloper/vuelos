/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.entities.Vuelo;
import com.mycompany.entities.Vuelo;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 
 */
public class Principal {

    private static final Logger LOG = Logger.getLogger(Principal.class.getName());

    /**
     * @param args the command line arguments
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        final EntityManagerFactory emf = Persistence.createEntityManagerFactory("JpaUnaClasePU");
        System.out.println("vuelos");
       
        Long clave = insertarPersona(emf);
        System.out.println("vuelo" + clave);
        System.out.println("Personas vuelos");
        List<Vuelo> personas = (List<Vuelo>) mostrarPersonas(emf);
        if (personas.isEmpty()) {
            System.out.println("No se encuentran personas");
        } else {
            for (Vuelo persona : personas) {
                System.out.println(persona);
            }
        }
        System.out.println("Intentando actualizar el nombre de la persona con clave " + clave);
        Vuelo p = actualizarPersona(clave, emf);
        System.out.println("Los datos de la nueva persona son " + p);
        System.out.println("Intentando borrar la persona con clave " + p);
        borrarPersona(p.getId(), emf);
        personas = (List<Vuelo>) mostrarPersonas(emf);
        if (personas.isEmpty()) {
            System.out.println("No se encuentran personas");
        } else {
            Iterable<Vuelo> vuelos = null;
            for (Vuelo vuelo : vuelos) {
                System.out.println(vuelo);
            }
        }
        emf.close();
    }

    static List<Vuelo> mostrarPersonas(EntityManagerFactory emf) {
        List<Vuelo> personas = null;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            TypedQuery<Vuelo> q = em.createQuery("select p from Persona p", Vuelo.class);
            personas = q.getResultList();
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            LOG.log(Level.ALL, "Ha ourrido un error", e);
            if (em != null) {
                em.getTransaction().rollback();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return personas;
    }

    static Long insertarPersona(EntityManagerFactory emf) {
        Long clave = null;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
          Vuelo p = null;//new Vuelo("abc");
            em.persist(p);
            em.getTransaction().commit();
            clave = p.getId();
        } catch (RuntimeException e) {
            LOG.log(Level.ALL, "Ha ourrido un error", e);
            if (em != null) {
                em.getTransaction().rollback();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return clave;
    }

    private static Vuelo actualizarPersona(Long clave, EntityManagerFactory emf) {
        Vuelo p = null;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
           
       //     p.setNombre("xyz");
            //Nota: la llamada al método merge es innecesaria
            p = em.merge(p);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            LOG.log(Level.ALL, "Ha ourrido un error", e);
            if (em != null) {
                em.getTransaction().rollback();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return p;
    }

    private static Vuelo borrarPersona(Long clave, EntityManagerFactory emf) {
        Vuelo p = null;
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            p = em.find(Vuelo.class, clave);
            em.remove(p);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            LOG.log(Level.ALL, "Ha ourrido un error", e);
            if (em != null) {
                em.getTransaction().rollback();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return p;
    }
    
}
