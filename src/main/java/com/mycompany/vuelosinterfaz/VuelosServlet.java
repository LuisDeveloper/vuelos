/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vuelosinterfaz;

import com.mycompany.vuelos.dao.DaoJPA;
import com.mycompany.entities.Vuelo;
import com.mycompany.vuelos.dao.Dao;
import com.mycompany.vuelos.dao.FakeDao;


import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Luis Criado
 */
@WebServlet(name = "VuelosServlet", urlPatterns = {"/vuelos"})
public class VuelosServlet extends HttpServlet{
private Dao dao;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("peticion recibida");
        System.out.println(req.getParameter("origenVuelo"));
        List<Vuelo> vuelos = dao.getVuelos(req.getParameter("origenVuelo"),req.getParameter("destinoVuelo"));
        req.setAttribute("vuelos", vuelos);
        req.getRequestDispatcher("vuelos.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
    }

    @Override
    public void init() throws ServletException {
       // dao = new FakeDao();
       dao = new DaoJPA();
    }
    
}
