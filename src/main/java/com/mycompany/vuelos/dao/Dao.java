/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vuelos.dao;

import com.mycompany.entities.Vuelo;
import java.util.List;

/**
 *
 * @author Luis Criado
 */
public interface Dao {
    public List<Vuelo> getVuelos(String origen,String destino);
    
    public Vuelo introducirVuelo (Vuelo v);
    
}
