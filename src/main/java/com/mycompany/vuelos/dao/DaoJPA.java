/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.vuelos.dao;

import com.mycompany.entities.Vuelo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author 0015512
 */
public class DaoJPA implements Dao {

    @Override
    public List<Vuelo> getVuelos(String origen, String destino) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("creaVuelo");
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
       List <Vuelo> ListaVuelos = em.createNamedQuery("buscaVuelos", Vuelo.class).setParameter("destino", destino).setParameter("origen", origen).getResultList();
        transaction.commit();
        emf.close();
        
        
        return ListaVuelos;
                
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vuelo introducirVuelo(Vuelo v) {
        
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("creaVuelo");
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        em.persist(v);
        transaction.commit();
        emf.close();
        return v;
    }
    
    
    
    
    
    
}
