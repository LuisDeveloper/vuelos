/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vuelos.dao;

import com.mycompany.entities.Vuelo;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Luis Criado
 */
public class FakeDao implements Dao{

   
    public List<Vuelo> getVuelos(String origen, String destino) {
        LinkedList<Vuelo> vuelos = new LinkedList<>();
        vuelos.add(new Vuelo(1000,origen,destino,new Date("2018-08-11")));
        vuelos.add(new Vuelo(1000,origen,destino,new Date("2018-08-11")));
        vuelos.add(new Vuelo(1000,origen,destino,new Date("2018-08-11")));
        vuelos.add(new Vuelo(1000,origen,destino,new Date("2018-08-11")));
        return vuelos;
    }

    @Override
    public Vuelo introducirVuelo(Vuelo v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
