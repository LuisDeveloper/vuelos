<%-- 
    Document   : vuelos
    Created on : 17-sep-2018, 17:41:56
    Author     : Luis Criado
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/vuelosCss.css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="cabecera">
            <div id="companyName">Burning Airlines,</div>
            <div>nuestros aviones nunca estallan en llamas</div>
        </div>
        <div class="container">
             <h1>Elija su viaje:</h1>
        <table>
            <thead>
            <th>origen</th><th>destino</th><th>precio</th><th>horaSalida</th>
            </thead>
        <c:forEach var="vuelo" items="${vuelos}">
            <tr>
                <td>${vuelo.origen}</td><td>${vuelo.destino}</td><td>${vuelo.precio}</td><td>${vuelo.horaSalida}</td>
            </tr>
        </c:forEach>
        </table>
        </div>
    </body>
</html>
